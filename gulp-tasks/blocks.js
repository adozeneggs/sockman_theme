/**
 *  See also:
 * - https://gulpjs.com/docs/en/getting-started/using-plugins
 * - https://github.com/rollup/rollup/issues/863
 */

module.exports = function(assetsPath) {
  const gulp = require('gulp');
  const path = require('path');
  const glob = require('glob');

  const rollup = require('rollup');
  const resolve = require('rollup-plugin-node-resolve');
  const commonjs = require('rollup-plugin-commonjs');
  const babel = require('rollup-plugin-babel');
  const { terser } = require('rollup-plugin-terser');

  const blocksDir = path.join(assetsPath, '/../inc/blocks/');
  const entries = glob.sync( path.join( blocksDir, '/*/block.js' ) );

  const rollupPlugins = [
    resolve({
      browser: true
    }),
    babel({
      extensions: ['.js'],
      presets: [
        [
          '@babel/env', {
            modules: false,
            targets: {
              browsers: [
                "last 2 Chrome versions",
                "last 2 Firefox versions",
                "last 2 Safari versions",
                "last 2 iOS versions",
                "last 1 Android version",
                "last 1 ChromeAndroid version",
                "ie 11"
              ]
            }
          },
        ]
      ],
      plugins: [
        ["@babel/plugin-transform-react-jsx", {
          "pragma": "wp.element.createElement"
        }]
      ]
    }),
    commonjs()
  ];

  function single_block_bundle_dev(entry) {
    const output = path.join(
      path.dirname(entry),
      'block.build.js'
    );

    const inputOptions = {
      input: entry,
      plugins: rollupPlugins
    };

    const outputOptions = {
      file: output,
      format: 'iife',
      sourcemap: true,
    };

    const watchOptions = {
      ...inputOptions,
      output: outputOptions,
      watch: {
        chokidar: true,
      }
    }

    const watcher = rollup.watch(watchOptions);

    watcher.on('event', function (event) {
      if (event.code === 'BUNDLE_END') {
        event.output.forEach(function (file) {
          console.log('bundled ' + file);
        });
      }
    });
  }

  function single_block_bundle_prod(entry) {
    const output = path.join(
      path.dirname(entry),
      'block.build.js'
    );

    const inputOptions = {
      input: entry,
      plugins: rollupPlugins.concat([
        terser()
      ])
    };

    const outputOptions = {
      file: output,
      format: 'iife',
    };

    return rollup.rollup(inputOptions)
      .then(bundle => {
        return bundle.write(outputOptions);
      })
      .then(bundle => {
        bundle.output.forEach(file => console.log(`bundled ${entry} as ${file.fileName}`));
      });
  }

  function blocks_dev() {
    return Promise.all(entries.map(single_block_bundle_dev));
  }

  function blocks_build() {
    return Promise.all(entries.map(single_block_bundle_prod));
  }

  return {
    dev: blocks_dev,
    build: blocks_build
  };
};
