/**
 *  See also:
 * - https://gulpjs.com/docs/en/getting-started/using-plugins
 * - https://github.com/rollup/rollup/issues/863
 */

module.exports = function (assetsPath) {
  const gulp = require('gulp');
  const path = require('path');
  const livereload = require('gulp-livereload');
  const glob = require('glob');
  const camelCase = require('camelcase');

  const rollup = require('rollup');
  const resolve = require('rollup-plugin-node-resolve');
  const commonjs = require('rollup-plugin-commonjs');
  const babel = require('rollup-plugin-babel');
  const { terser } = require('rollup-plugin-terser');

  const rev = require('gulp-rev');

  const srcDir = path.join(assetsPath, '/src/js/');
  const src = path.join(srcDir, '/**/*.js');
  const dist = path.join(assetsPath, '/dist/js/');

  const jsManifest = path.join(assetsPath, '/dist/js/js-manifest.json');

  const outputs = [
    path.join(dist, '/*.js')
  ];

  const entries = glob.sync(path.join(srcDir, '/*.js'), {
    ignore: path.join(srcDir, '/**/_*.js')
  });

  function scripts_dev() {
    return Promise.all(entries.map(single_bundle_dev));
  }

  function single_bundle_dev(entry) {
    const filename = path.basename(entry);
    const output = path.join(dist, filename);
    const outputName = camelCase(path.basename(entry, '.js'));

    const inputOptions = {
      input: entry,
      plugins: [
        resolve({
          browser: true
        }),
        commonjs(),
      ]
    };

    const outputOptions = {
      file: output,
      format: 'iife',
      sourcemap: true,
      name: outputName
    };

    const watchOptions = {
      ...inputOptions,
      output: outputOptions,
      watch: {
        chokidar: true,
      }
    }

    const watcher = rollup.watch(watchOptions);

    watcher.on('event', function (event) {
      if (event.code === 'BUNDLE_END') {
        event.output.forEach(function (file) {
          livereload.reload(file);
        });
      }
      if (event.code === 'ERROR') {
        console.log(event.error);
      }
    });
  }

  function scripts_build() {
    return Promise.all(entries.map(single_bundle_prod))
      .then(revJs);
  }

  function single_bundle_prod(entry) {
    const filename = path.basename(entry);
    const output = path.join(dist, filename);
    const outputName = camelCase(path.basename(entry, '.js'));

    const inputOptions = {
      input: entry,
      plugins: [
        resolve({
          browser: true
        }),
        babel({
          extensions: ['.js', '.mjs', '.html', '.svelte'],
          presets: [
            [
              '@babel/env', {
                'modules': false,
                'targets': {
                  'chrome': '64',
                  'ie': '11'
                }
              }
            ]
          ],
          exclude: 'node_modules/**'
        }),
        commonjs(),
        terser()
      ]
    };

    const outputOptions = {
      file: output,
      format: 'iife',
      sourcemap: true,
      name: outputName
    };

    return rollup.rollup(inputOptions)
      .then(bundle => {
        return bundle.write(outputOptions)
      })
      .then(bundle => {
        bundle.output.forEach(file => console.log(`bundled ${file.fileName}`));
      });
  }

  function revJs() {
    return gulp.src(outputs)
      .pipe(rev())
      .pipe(gulp.dest(dist))
      .pipe(rev.manifest(jsManifest, {
        base: assetsPath,
        merge: true
      }))
      .pipe(gulp.dest(assetsPath));
  }

  return {
    dev: scripts_dev,
    build: scripts_build
  };
};
