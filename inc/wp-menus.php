<?php
/**
 * Menu locations.
 *
 * Hooked to 'init' action.
 *
 * @package Sockman
 */

register_nav_menus(
	array(
		'main-menu' => __( 'Main Menu' ),
		'legal-menu' => __( 'Legal Menu' ),
	)
);
