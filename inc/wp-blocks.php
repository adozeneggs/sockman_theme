<?php
/**
 * Blocks. Hooked in to acf/init, just declare the `acf_register_block_type` s.
 *
 * Documentation here: https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 * @package Sockman
 */

/**
 * ACF Gutenberg blocks render callback.
 *
 * @param   array  $block      The block settings and attributes.
 * @param   string $content    The block content (emtpy string).
 * @param   bool   $is_preview True during AJAX preview.
 */
function sockman_blocks_render_callback( $block, $content = '', $is_preview = false ) {
	$context = Timber::get_context();

	// Convert name ("acf/testimonial") into path friendly slug ("testimonial").
	$slug = str_replace( 'acf/', '', $block['name'] );

	// Store block values.
	$context['block'] = $block;

	// Store field values.
	$context['fields'] = get_fields();

	// Store $is_preview value.
	$context['is_preview'] = $is_preview;

	// Render the block.
	Timber::render( 'blocks/blocks.' . $slug . '.twig', $context );
}

acf_register_block_type(
	array(
		'name'            => 'link-with-arrow',
		'title'           => __( 'Link' ),
		'description'     => __( 'Link with right arrow.' ),
		'render_callback' => 'sockman_blocks_render_callback',
		'category'        => 'common',
		'icon'            => 'admin-links',
		'post_types'      => array( 'page' ),
		'keywords'        => array( 'link' ),
		'mode'            => 'edit',
		'align'           => '',
		'supports'        => array(
			// 'align'		=> array( 'wide' ),
		),
	)
);
