const { __, setLocaleData } = wp.i18n;
const {
  registerBlockType,
} = wp.blocks;
const {
  RichText,
  MediaUpload,
} = wp.blockEditor;
const { Button } = wp.components;

registerBlockType( 'sockman/subtitle', {
  title: 'Subtitle',
  icon: <svg viewBox="0 0 24 24"><path d="M4,9H20V11H4V9M4,13H14V15H4V13Z" /></svg>,
  category: 'common',
  attributes: {
    subtitle: {
      type: 'string',
      source: 'meta',
      meta: 'subtitle',
    },
  },
  supports: {
    multiple: false,
    reusable: false,
  },
  edit: ( { attributes, className, setAttributes } ) => {
    const { subtitle } = attributes;
    
    const onChangeSubtitle = (value) => {
      setAttributes({ subtitle: value });
    };

    return (
      <RichText
        tagName="p"
        className={ className }
        onChange={ onChangeSubtitle }
        value={ subtitle }
      />
    );
  },
  save: function() {
    // rendering in php/twig.
    return null;
  }
} );
