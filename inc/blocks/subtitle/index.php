<?php
/**
 * Subtitle block.
 *
 * @package  sockman
 */

/**
 * Renders the `sockman/subtitle` block on server.
 *
 * @param array $attributes The block attributes.
 *
 * @return string Returns the post content with latest posts added.
 */
function render_block_sockman_subtitle() {
	// echo get_post_meta( 'subtitle' );
}

/**
 * Registers all block assets.
 */
function register_block_sockman_subtitle() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// block editor is not active.
		return;
	}

	// only allow one subtitle to be saved.
	register_meta(
		'post',
		'subtitle',
		array(
			'show_in_rest' => true,
			'single'       => true,
			'type'         => 'string',
		)
	);

	wp_register_script(
		'sockman-subtitle',
		get_template_directory_uri() . '/inc/blocks/subtitle/block.build.js',
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' ),
		filemtime( get_template_directory() . '/inc/blocks/subtitle/block.build.js' )
	);

	wp_register_style(
		'sockman-subtitle',
		get_template_directory_uri() . '/inc/blocks/subtitle/style.css',
		array(),
		filemtime( get_template_directory() . '/inc/blocks/subtitle/style.css' )
	);

	register_block_type(
		'sockman/subtitle',
		array(
			'editor_style'  => 'sockman-subtitle',
			'editor_script' => 'sockman-subtitle',
		)
	);

}

add_action( 'init', 'register_block_sockman_subtitle' );
