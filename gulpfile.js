"use strict";

// theme options
const assetsPath = 'assets/';
const themeName = 'sockman';
const options = {
  appName: 'sockman',
  bgColor: '#fff',
  themeColor: '#fff'
};
const livereloadPort = '35729';

// imports
const gulp = require('gulp');
const livereload = require('gulp-livereload');

// tasks
const assets = require('./gulp-tasks/assets')(assetsPath);
const svgs = require('./gulp-tasks/svgs')(assetsPath);
const styles = require('./gulp-tasks/styles')(assetsPath);
const scripts = require('./gulp-tasks/scripts')(assetsPath);
const blocks = require('./gulp-tasks/blocks')(assetsPath);
const favicons = require('./gulp-tasks/favicons')(assetsPath, options, themeName);

function livereload_listen() {
  livereload.listen({
    host: '0.0.0.0',
    port: livereloadPort
  });
}

exports.default = gulp.series(
  assets.clean,
  gulp.parallel(
    livereload_listen,
    gulp.series(
      gulp.parallel(
        assets.copy,
        svgs.optimize,
        styles.dev
      ),
      gulp.parallel(
        assets.watch,
        svgs.watch,
        styles.watch,
        scripts.dev,
        blocks.dev,
      )
    )
  )
);

exports.build = gulp.series(
  assets.clean,
  gulp.parallel(
    assets.copy,
    svgs.optimize,
    styles.build,
    scripts.build,
    blocks.build
  )
);

exports.favicons = favicons;

