<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package  sockman
 */

$context = Timber::get_context();
Timber::render( '404.twig', $context );
